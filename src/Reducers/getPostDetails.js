let initialState = {
  data: '',
};
const postDetailsReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case 'POST_DETAILS':
      console.log(payload, 'from  postDetailsReducer reducer');
      return {
        ...state,
        data: payload,
      };
    default:
      return state;
  }
};

export default postDetailsReducer;
