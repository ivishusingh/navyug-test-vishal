let initialState = {
  data: [],
};
const detailsReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case 'USER_DETAILS':
      console.log(payload, 'from details reducer');
      return {
        ...state,
        data: payload,
      };
    default:
      return state;
  }
};

export default detailsReducer;
