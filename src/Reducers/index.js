import { combineReducers } from 'redux';
import listReducer from './getListingReducer';
import detailsReducer from './getDetails';
import postsReducer from './getPost';
import commentsReducer from './getPostComments';
import postDetailsReducer from './getPostDetails';

const rootReducer = combineReducers({
  listReducer,
  detailsReducer,
  postsReducer,
  commentsReducer,
  postDetailsReducer,
});

export default rootReducer;
