let initialState = {
  data: [],
};
const commentsReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case 'POST_COMMENTS':
      console.log(payload, 'from reducer');
      return {
        ...state,
        data: payload,
      };

    case 'ADDCOMMENT_ACTION':
      console.log(payload, 'from reducer add comment');
      state.data.unshift(payload);
      return {
        ...state,
        data: state.data,
      };
    default:
      return state;
  }
};

export default commentsReducer;
