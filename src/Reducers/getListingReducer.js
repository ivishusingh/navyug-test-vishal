let initialState = {
  data: [],
};
const listReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case 'USER_DATA':
      console.log(payload, 'from reducer');
      return {
        ...state,
        data: payload,
      };
    default:
      return state;
  }
};

export default listReducer;
