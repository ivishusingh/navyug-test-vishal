let initialState = {
  data: [],
};
const postsReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case 'USER_POST':
      console.log(payload, 'from post reducer');
      return {
        ...state,
        data: payload,
      };
    case 'DELETE_ACTION':
      console.log(payload, 'from delete reducer');
      // let res = [...state.data];
      state.data.splice(payload.i, 1);
      console.log(state.data);
      return {
        ...state,
        data: state.data,
      };
    case 'ADDPOST_ACTION':
      console.log(payload);
      state.data.unshift(payload);
      return {
        ...state,
        data: state.data,
      };

    default:
      return state;
  }
};

export default postsReducer;
