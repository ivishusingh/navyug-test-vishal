import axios from 'axios';

const userDetails = (id) => {
  return async (dispatch) => {
    let result = await axios.get(
      `https://jsonplaceholder.typicode.com/users/${id}`
    );
    console.log(result, 'inside action');
    if (result.status == 200) {
      dispatch({ type: 'USER_DETAILS', payload: result.data });
    } else {
      dispatch({ type: 'USER_DETAILS', payload: {} });
    }
  };
};
export default userDetails;
