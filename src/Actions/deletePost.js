// import axios from 'axios';

const deletePost = (id, i) => {
  // i am writing this code above, because placeholder api's dont delete in actual so there is no need for wait api reponse
  return (dispatch) => {
    dispatch({ type: 'DELETE_ACTION', payload: { id, i } });
    alert('deleted');
  };
  // let result = await axios.delete(
  //   `https://jsonplaceholder.typicode.com/posts/${id}`
  // );
  // if (result.status == 200) {
  //   console.log('deleted');
  // } else {
  //   dispatch({ type: 'DELETE_ACTION', payload: {} });
  // }
};
export default deletePost;
