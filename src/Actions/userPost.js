import axios from 'axios';

const userPosts = (id) => {
  return async (dispatch) => {
    let result = await axios.get(
      `https://jsonplaceholder.typicode.com/posts?userId=${id}`
    );
    console.log(result, 'inside action');
    if (result.status == 200) {
      dispatch({ type: 'USER_POST', payload: result.data });
    } else {
      dispatch({ type: 'USER_POST', payload: {} });
    }
  };
};
export default userPosts;
