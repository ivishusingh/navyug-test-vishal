// import axios from 'axios';

const addPost = (title, body) => {
  return (dispatch) => {
    let bodyTOSend = {
      title,
      body,
      id: 'custom',
    };

    dispatch({ type: 'ADDPOST_ACTION', payload: bodyTOSend });

    // let result = await axios.post(
    //   `https://jsonplaceholder.typicode.com/posts`,
    //   bodyTOSend
    // );
    // console.log(result, 'inside action');
    // if (result.status == 200 || 201) {
    //   console.log('added');
    // } else {
    //   console.log('error');
    // }
  };
};
export default addPost;
