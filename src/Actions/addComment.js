import axios from 'axios';

const addComment = (name, email, body) => {
  return async (dispatch) => {
    let bodyToSend = {
      name,
      email,
      body,
    };

    dispatch({ type: 'ADDCOMMENT_ACTION', payload: bodyToSend });
    // i was not able to find the post comments api
    // let result = await axios.post(
    //   `https://jsonplaceholder.typicode.com/posts`,
    //   bodyToSend
    // );
    // console.log(result, 'inside action');
    // if (result.status == 200 || 201) {
    //   console.log('added');
    // } else {
    //   console.log('error');
    // }
  };
};
export default addComment;
