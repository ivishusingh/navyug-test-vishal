import axios from 'axios';

const userListing = () => {
  return async (dispatch) => {
    let result = await axios.get(`https://jsonplaceholder.typicode.com/users`);
    console.log(result, 'inside action');
    if (result.status == 200) {
      dispatch({ type: 'USER_DATA', payload: result.data });
    } else {
      dispatch({ type: 'USER_DATA', payload: {} });
    }
  };
};
export default userListing;
