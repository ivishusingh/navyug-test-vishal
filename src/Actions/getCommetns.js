import axios from 'axios';

const getComments = (id) => {
  return async (dispatch) => {
    let result = await axios.get(
      `https://jsonplaceholder.typicode.com/posts/${id}/comments`
    );
    console.log(result, 'inside POST_COMMENTS action');
    if (result.status == 200) {
      dispatch({ type: 'POST_COMMENTS', payload: result.data });
    } else {
      dispatch({ type: 'POST_COMMENTS', payload: {} });
    }
  };
};
export default getComments;
