import axios from 'axios';

const getPostDetails = (id) => {
  return async (dispatch) => {
    let result = await axios.get(
      `https://jsonplaceholder.typicode.com/posts/${id}`
    );
    console.log(result, 'inside POST action');
    if ((id == 'custom', result.status == 200)) {
      dispatch({ type: 'POST_DETAILS', payload: result.data });
    } else {
      dispatch({ type: 'POST_DETAILS', payload: id });
    }
  };
};
export default getPostDetails;
