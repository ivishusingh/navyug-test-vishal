import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Home from './components/Home';
import Navbar from './components/Navbar';
import Details from './components/Details';
import PostDetails from './components/PostDetails';
import Routes from './routes';

export default class App extends Component {
  render() {
    return (
      <div>
        <Routes />
        {/* <Navbar />
        <BrowserRouter>
          <Switch>
            <Route exact path="/app/user" component={Home} />
            <Route exact path="/app/user/:userId" component={Details} />
            <Route
              exact
              path="/app/user/:userid/:postId"
              component={PostDetails}
            />
          </Switch>
        </BrowserRouter> */}
      </div>
    );
  }
}
