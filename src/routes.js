import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Home from './components/Home';
import Navbar from './components/Navbar';
import Details from './components/Details';
import PostDetails from './components/PostDetails';
import Dashboard from './components/Dashboard';

import React from 'react';

export default function routes() {
  return (
    <div>
      <Navbar />
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Dashboard} />
          <Route exact path="/app/user" component={Home} />
          <Route exact path="/app/user/:userId" component={Details} />
          <Route
            exact
            path="/app/user/:userid/:postId"
            component={PostDetails}
          />
        </Switch>
      </BrowserRouter>
    </div>
  );
}
