import React, { Component } from 'react';
import getComments from '../../Actions/getCommetns';

import getPostDetails from '../../Actions/getPostDeatils';
import addComment from '../../Actions/addComment';
import { connect } from 'react-redux';
import Loader from '../Loader';
import { Link } from 'react-router-dom';

class index extends Component {
  state = {
    viewComments: false,
    name: '',
    email: '',
    body: '',
  };
  componentDidMount() {
    let postId = this.props.match.params.postId;

    this.props.getComments(postId);
    this.props.getPostDetails(postId);
  }
  ViewComments = () => {
    this.setState((prevState) => ({
      viewComments: !prevState.viewComments,
    }));
  };
  addComment = (e) => {
    e.preventDefault();
    let mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    const { name, email, body } = this.state;
    if (!name) {
      alert('enter name');
    } else if (!email.match(mailformat)) {
      alert('enter valid email');
    } else if (!body) {
      alert('enter body');
    } else {
      this.props.addComment(name, email, body);
      this.resetData(e);
    }
  };
  resetData = (e) => {
    e.preventDefault();
    this.setState({ name: '', email: '', body: '' });
  };
  render() {
    console.log(this.props, 'from post');
    return (
      <div>
        <div
          class="modal fade"
          id="exampleModal"
          tabindex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                  Add Comment
                </h5>
                <button
                  type="button"
                  class="close"
                  data-dismiss="modal"
                  aria-label="Close"
                  onClick={(e) => this.resetData(e)}
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label">
                      Name
                    </label>
                    <div class="col-sm-10">
                      <input
                        onChange={(e) => {
                          this.setState({ name: e.target.value });
                        }}
                        value={this.state.name}
                        type="text"
                        class="form-control"
                        id="staticEmail"
                      />
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label">
                      Email
                    </label>
                    <div class="col-sm-10">
                      <input
                        onChange={(e) => {
                          this.setState({ email: e.target.value });
                        }}
                        value={this.state.email}
                        type="text"
                        class="form-control"
                        id="staticEmail"
                      />
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label">
                      Body
                    </label>
                    <div class="col-sm-10">
                      <textarea
                        value={this.state.body}
                        onChange={(e) => {
                          this.setState({ body: e.target.value });
                        }}
                        class="form-control"
                        id="exampleFormControlTextarea1"
                        rows="3"
                      ></textarea>
                    </div>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button
                  type="button"
                  class="btn btn-secondary"
                  data-dismiss="modal"
                  onClick={(e) => this.resetData(e)}
                >
                  Close
                </button>
                <button
                  type="button"
                  class="btn btn-primary"
                  onClick={(e) => this.addComment(e)}
                  data-dismiss="modal"
                >
                  Save changes
                </button>
              </div>
            </div>
          </div>
        </div>

        {!this.props.data.postDetailsReducer ? (
          <Loader />
        ) : (
          <div className="container">
            <div className="row m-2 ">
              <div className="col-4">
                <Link to={`/app/user/${this.props.match.params.userid}`}>
                  <button className="btn btn-outline-warning">GO BACK</button>
                </Link>
              </div>
              <div className="col-4">
                <h2>{this.props.data.detailsReducer.data.name}</h2>
              </div>
              <div className="col-4"></div>
            </div>
            <div className="row mt-4 text-center">
              <div className="col-12">
                <h3>{this.props.data.postDetailsReducer.data.title}</h3>
              </div>
            </div>
            <div className="row mt-2">
              <div className="col-12">
                <p>{this.props.data.postDetailsReducer.data.body}</p>
              </div>
            </div>
            <div className="row mt-2">
              <div className="col-2 ">
                <button
                  className="btn btn-outline-primary"
                  onClick={(e) => this.ViewComments(true)}
                >
                  {!this.state.viewComments
                    ? ' View Comments'
                    : 'Hide Comments'}
                </button>
              </div>
              <div className="col-2 ">
                <button
                  className="btn btn-outline-success text-right"
                  data-toggle="modal"
                  data-target="#exampleModal"
                >
                  Add Comment
                </button>
              </div>
            </div>
            {this.state.viewComments ? (
              <div className="row mt-2 ">
                <div className="col-12">
                  {this.props.data.commentsReducer.data.map((item) => {
                    return (
                      <div class="card m-1 shadow ">
                        <div class="card-body">
                          <h4>{item.name}</h4>
                          <p>{item.body}</p>
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
            ) : null}
          </div>
        )}
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return { data: state };
};
export default connect(mapStateToProps, {
  getComments,
  getPostDetails,
  addComment,
})(index);
