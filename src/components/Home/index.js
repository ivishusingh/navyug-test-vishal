import React, { Component } from 'react';
import userListing from '../../Actions/userListing';
import { connect } from 'react-redux';
import Loader from '../Loader';
import { Link } from 'react-router-dom';

class index extends Component {
  componentDidMount() {
    this.props.userListing();
  }
  render() {
    console.log(this.props, 'from render');
    return (
      <div>
        {!this.props.data.listReducer.data.length ? (
          <Loader />
        ) : (
          <div className="row m-2">
            {this.props.data.listReducer.data.map((item) => {
              return (
                <div className="col-3 p-2">
                  <div className="card shadow" style={{ width: '18rem' }}>
                    <div className="card-header bg-dark text-white ">
                      {item.name}
                    </div>
                    <div className="card-body">
                      <h5 className="card-title">{item.name}</h5>
                      <h6 className="card-subtitle mb-2 text-muted">
                        {item.email}
                      </h6>
                      <p className="card-text">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        A, nobis odit. Temporibus tempora numquam dolorem
                      </p>
                      <div className="row text-center ">
                        <div className="col">
                          <Link to={`${this.props.match.path}/${item.id}`}>
                            <button className="btn btn-outline-success">
                              Details
                            </button>
                          </Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  // console.log()
  return { data: state };
};
export default connect(mapStateToProps, { userListing })(index);
