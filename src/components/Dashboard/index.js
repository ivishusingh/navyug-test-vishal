import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class index extends Component {
  render() {
    return (
      <div>
        <div class="jumbotron jumbotron-fluid">
          <div class="container">
            <h1 class="display-4">Hi , Welcome To Test Task </h1>
            <Link to={`/app/user`}>
              <button className="btn btn-outline-success">Let's Start</button>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}
