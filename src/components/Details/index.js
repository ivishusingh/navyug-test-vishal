import React, { Component } from 'react';
import userDetails from '../../Actions/userDetails';
import userPosts from '../../Actions/userPost';
import deletePost from '../../Actions/deletePost';
import addPost from '../../Actions/addPost';
import { connect } from 'react-redux';
import Loader from '../Loader';
import { Link } from 'react-router-dom';

class index extends Component {
  state = {
    title: '',
    body: '',
  };

  componentDidMount() {
    let id = this.props.match.params.userId;
    this.props.userDetails(id);
    this.props.userPosts(id);
  }
  deleteModal = (e, id, i) => {
    e.preventDefault();
    this.props.deletePost(id, i);
    console.log(id);
  };
  submitPost = (e) => {
    const { title, body } = this.state;
    e.preventDefault();
    if (!title) {
      return alert('please enter title');
    } else if (!body) {
      return alert('please enter body');
    } else {
      this.props.addPost(title, body);
      this.resetState(e);
    }
  };
  resetState = () => {
    this.setState({ title: '', body: '' });
  };
  render() {
    console.log(this.props, 'from detils');
    return (
      <div>
        {!this.props.detailsReducer.data &&
        !this.props.postsReducer.data.length ? (
          <Loader />
        ) : (
          <div>
            {/* add post modal */}
            <div
              tabindex="-1"
              role="dialog"
              className="modal fade"
              id="exampleModal"
              tabindex="-1"
              role="dialog"
              aria-labelledby="exampleModalLabel"
              aria-hidden="true"
            >
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title">Add Post</h5>
                    <button
                      type="button"
                      class="close"
                      data-dismiss="modal"
                      aria-label="Close"
                      onClick={(e) => this.resetState(e)}
                    >
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form>
                      <div class="form-group row">
                        <label
                          for="staticEmail"
                          class="col-sm-2 col-form-label"
                        >
                          Title
                        </label>
                        <div class="col-sm-10">
                          <input
                            onChange={(e) => {
                              this.setState({ title: e.target.value });
                            }}
                            type="text"
                            class="form-control"
                            id="staticEmail"
                          />
                        </div>
                      </div>
                      <div class="form-group row">
                        <label
                          for="inputPassword"
                          class="col-sm-2 col-form-label"
                        >
                          Body
                        </label>
                        <div class="col-sm-10">
                          <textarea
                            onChange={(e) => {
                              this.setState({ body: e.target.value });
                            }}
                            class="form-control"
                            id="exampleFormControlTextarea1"
                            rows="3"
                          ></textarea>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button
                      type="button"
                      class="btn btn-secondary"
                      data-dismiss="modal"
                      onClick={(e) => this.resetState(e)}
                    >
                      Close
                    </button>
                    <button
                      onClick={(e) => this.submitPost(e)}
                      type="button"
                      class="btn btn-primary"
                      data-dismiss="modal"
                      aria-label="Close"
                    >
                      Save changes
                    </button>
                  </div>
                </div>
              </div>
            </div>

            <div className="container mt-5">
              <div className="row text-center ">
                <div className="col-4">
                  <Link to={`/app/user`}>
                    <button className="btn btn-outline-warning">Go Back</button>
                  </Link>
                </div>
                <div className="col-4">
                  <h3>{this.props.detailsReducer.data.name}</h3>
                </div>
                <div className="col-4">
                  <button
                    className="btn btn-outline-success"
                    data-toggle="modal"
                    data-target="#exampleModal"
                  >
                    Add Post
                  </button>
                </div>
              </div>
              <div className="row mt-3">
                <div className="col-12">
                  <table class="table">
                    <thead class="thead-dark">
                      <tr>
                        <th>Delete</th>
                        <th scope="col">Title</th>
                        <th>View</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.props.postsReducer.data.map((item, i) => {
                        return (
                          <tr>
                            <th scope="row">
                              <button
                                onClick={(e) => this.deleteModal(e, item.id, i)}
                                className="btn btn-outline-danger"
                              >
                                Delete
                              </button>
                            </th>
                            <th scope="row">
                              <td>{item.title}</td>
                            </th>
                            <td>
                              <th scope="row">
                                {item.id != 'custom' ? (
                                  <Link
                                    to={`${this.props.match.params.userId}/${item.id}`}
                                  >
                                    <button
                                      // onClick={}
                                      className="btn btn-outline-info"
                                    >
                                      View Details
                                    </button>
                                  </Link>
                                ) : null}
                              </th>
                            </td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return state;
};
export default connect(mapStateToProps, {
  userDetails,
  userPosts,
  deletePost,
  addPost,
})(index);
